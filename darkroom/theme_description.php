<?php
// Zenphoto theme definition file
$theme_description["name"] = ucfirst(basename(dirname(__FILE__)));
$theme_description["author"] = _('<a href="https://robiotic.net" target="_blank">Klikini</a>');
$theme_description["version"] = "0.0.1";
$theme_description["date"] = "2018-06-06";
$theme_description["desc"] = _("A dark and modern (for 2018) theme. Requires PHP 7.");

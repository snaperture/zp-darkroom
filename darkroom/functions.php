<?php

include_once("themeoptions.php");
require_once("filters.php");

enableExtension('zenpage', 0, false);

function body_classes() {
	$scheme = getOption(ThemeOptions::OPTION_COLOR_SCHEME) ? "light" : "dark";
	$placement = getOption(ThemeOptions::OPTION_NAVBAR_PLACEMENT) ? "top" : "bottom";
	return "bg-$scheme navbar-$placement";
}

<?php
	if (!defined("WEBPATH")) {
		die();
	}
?>
<!doctype html>
<html>
	<head>
		<?php
			zp_apply_filter("theme_head");

			if (class_exists("RSS")) {
				printRSSHeaderLink("Gallery", _("Gallery RSS"));
			}
		?>
	</head>

	<body class="<?= body_classes() ?>">
		<?php zp_apply_filter("theme_body_open"); ?>

		<!-- Carousel -->
		<?php
			require_once("carousel.php");

			$featured_album_name = getOption(ThemeOptions::OPTION_FEATURED_ALBUM);

			if (!empty($featured_album_name)) {
				$featured_album = new dynamicAlbum($featured_album_name);
				make_carousel("featured", $featured_album);
			}
		?>

		<!-- Index -->
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<p class="lead"><?php printGalleryDesc(); ?></p>
				</div>
			</div>

			<!-- Albums -->
			<div class="row">
				<?php while (next_album()): if ($_zp_current_album->getFolder() == getOption(ThemeOptions::OPTION_FEATURED_ALBUM)) continue; ?>
					<div class="col-md-<?= ceil(12 / getOption("albums_per_row")) ?>">
						<div class="card mb-4 box-shadow card-<?= getOption(ThemeOptions::OPTION_COLOR_SCHEME) ? "light" : "dark" ?>">
							<img class="card-img-top" src="<?= getAlbumThumb() ?>">
							<div class="card-body">
								<div class="card-text d-flex justify-content-between align-items-center">
									<p class="lead"><?= getAlbumTitle() ?></p>
									<?php if (getOption(ThemeOptions::OPTION_ITEM_TYPES)): ?>
										<p class="lead">
											<small class="text-muted"><?= _("Album") ?></small>
										</p>
									<?php endif; ?>
								</div>
								<p class="card-text"><?= getAlbumDesc(); ?></p>
								<div class="d-flex justify-content-between align-items-center">
									<div class="btn-group">
										<a class="btn btn-sm btn-outline-secondary" href="<?= getAlbumURL() ?>">View</a>
										<?php if ($_zp_current_album->isMyItem(ALBUM_RIGHTS)): ?>
											<a class="btn btn-sm btn-outline-secondary" href="/zp-core/admin-edit.php?page=edit&album=<?= $_zp_current_album->name ?>">Edit</a>
										<?php endif; ?>
									</div>
									<small class="text-muted"><?= getAlbumDate() ?></small>
								</div>
							</div>
						</div>
					</div>
				<?php endwhile; ?>
			</div>

			<!-- Pagination -->
			<?php if (getTotalPages() > 1): ?>
				<div class="row">
					<div class="col-md-12 text-center">
						<ul class="pagination">
						<?php
							$nav = getPageNavList(false, 0, false, getCurrentPage(), max(1, getTotalPages()));
							foreach ($nav as $name => $url) {
								?>
								<li class="page-item <?= getCurrentPage() == $name ? "active" : null ?> <?= $nav[$name] == null ? "disabled" : null ?>">
									<a class="page-link other-<?= getOption(ThemeOptions::OPTION_COLOR_SCHEME) ? "light" : "dark" ?>" href="<?= $nav[$name] == null ? "#" : "." . $url ?>">
										<?php if ($name == "prev") { ?>
											<span aria-hidden="true">&laquo;</span>
											<span class="sr-only">Previous</span>
										<?php } else if ($name == "next") { ?>
											<span aria-hidden="true">&raquo;</span>
											<span class="sr-only">Next</span>
										<?php } else { echo($name); } ?>
									</a>
								</li>
								<?php
							}
						?>
						</ul>
					</div>
				</div>
			<?php endif; ?>
		</div>

		<?php zp_apply_filter("theme_body_close"); ?>
	</body>
</html>
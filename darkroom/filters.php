<?php

zp_register_filter("theme_head", "do_head_meta", 999999999);
zp_register_filter("theme_head", "do_head_style");
zp_register_filter("theme_body_open", "do_body_open");
zp_register_filter("theme_body_close", "do_body_close");

function do_head_meta() {
	?>
	<meta charset="<?= LOCAL_CHARSET ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<?php

	printHeadTitle();
}

function do_head_style() {
	global $_zp_themeroot;

	?>
	<!-- CSS -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
	<link rel="stylesheet" href="<?= $_zp_themeroot . "/styles.css" ?>" type="text/css" />
	<?php
}

function do_body_open() {
	global $_zp_gallery;
	global $_zp_current_album;
	global $_zp_current_image;
	$homepage = $_zp_current_album == null && $_zp_current_image == null;

	?>
	<!-- Navbar -->
	<nav class="navbar fixed-<?= getOption(ThemeOptions::OPTION_NAVBAR_PLACEMENT) ? "top" : "bottom" ?> <?= getOption(ThemeOptions::OPTION_COLOR_SCHEME) ? "navbar-light bg-light" : "navbar-dark bg-dark" ?> navbar-expand-md box-shadow">
		<ol class="breadcrumb">
			<?php if ($homepage): ?>
				<li class="breadcrumb-item activce">
					<?php if (getOption(ThemeOptions::OPTION_ITEM_TYPES)): ?>
						<small class="text-muted"><?= _("Gallery") ?></small>
					<?php endif; ?>
					<?= $_zp_gallery->getWebsiteTitle() ?>
				</li>
			<?php else: ?>
				<li class="breadcrumb-item">
					<?php if (getOption(ThemeOptions::OPTION_ITEM_TYPES)): ?>
						<small class="text-muted"><?= _("Gallery") ?></small>
					<?php endif; ?>
					<a href="<?= $_zp_gallery->getWebsiteURL() ?>"><?= $_zp_gallery->getWebsiteTitle() ?></a>
				</li>
			<?php endif; ?>

			<?php if ($_zp_current_album): ?>
				<?php $parent_album = $_zp_current_album; ?>
				<?php while ($parent_album = $parent_album->getParent()): ?>
					<li class="breadcrumb-item">
						<?php if (getOption(ThemeOptions::OPTION_ITEM_TYPES)): ?>
							<small class="text-muted"><?= _("Album") ?></small>
						<?php endif; ?>
						<a href="<?= $parent_album->getLink() ?>"><?= $parent_album->getTitle() ?></a>
					</li>
				<?php endwhile; ?>

				<?php if ($_zp_current_image): ?>
					<li class="breadcrumb-item">
						<?php if (getOption(ThemeOptions::OPTION_ITEM_TYPES)): ?>
							<small class="text-muted"><?= _("Album") ?></small>
						<?php endif; ?>
						<a href="<?= $_zp_current_album->getLink() ?>"><?= getAlbumTitle() ?></a>
					</li>
				<?php else: ?>
					<li class="breadcrumb-item active">
						<?php if (getOption(ThemeOptions::OPTION_ITEM_TYPES)): ?>
							<small class="text-muted"><?= _("Album") ?></small>
						<?php endif; ?>
						<?= getAlbumTitle() ?>
					</li>
				<?php endif; ?>
			<?php endif; ?>

			<?php if ($_zp_current_image): ?>
				<li class="breadcrumb-item active">
					<?php if (getOption(ThemeOptions::OPTION_ITEM_TYPES)): ?>
						<small class="text-muted"><?= _("Image") ?></small>
					<?php endif; ?>
					<?= getImageTitle() ?>
				</li>
			<?php endif; ?>
		</ol>

		<?php
		//			if (extensionEnabled("contact_form")) {
		//				printCustomPageURL(_("Contact"), "contact", "", "", " | ");
		//			}

		//			if (!zp_loggedin() && function_exists("printRegisterURL")) {
		//				printRegisterURL(gettext("Register"), "", " | ");
		//			}

		//			if (getOption("Allow_search")) {
		//				printSearchForm();
		//			}

		//			@call_user_func("printUserLogin_out", "", " | ");

		//  		if (function_exists("printFavoritesURL")) {
		//			    printFavoritesURL(NULL, "", " | ", '<br />');
		//		    }
		?>
	</nav>
	<?
}

function do_body_close() {
	?>
	<!-- Bootstrap JS -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
	<?php
}

<?php
if (!defined("WEBPATH")) {
	die();
}
?>
<!doctype html>
<html>
	<head>
		<?php
		zp_apply_filter("theme_head");

		if (class_exists("RSS")) {
			printRSSHeaderLink("Album", getAlbumTitle());
		}
		?>
	</head>

	<body class="<?= body_classes() ?>">
		<?php zp_apply_filter("theme_body_open"); ?>

		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<p class="lead"><?php printAlbumDesc(); ?></p>
				</div>
			</div>

			<!-- Albums -->
			<div class="row">
				<?php while (next_album()): ?>
					<div class="col-md-<?= ceil(12 / getOption("albums_per_row")) ?>">
						<div class="card mb-4 box-shadow card-<?= getOption(ThemeOptions::OPTION_COLOR_SCHEME) ? "light" : "dark" ?>">
							<img class="card-img-top"
							     src="<?= getCustomAlbumThumb(350, 350, 350, 350, 350, null, null, null) ?>">
							<div class="card-body">
								<div class="card-text d-flex justify-content-between align-items-center">
									<p class="lead"><?= getAlbumTitle() ?></p>
									<?php if (getOption(ThemeOptions::OPTION_ITEM_TYPES)): ?>
										<p class="lead">
											<small class="text-muted"><?= _("Album") ?></small>
										</p>
									<?php endif; ?>
								</div>
								<p class="card-text"><?= getAlbumDesc() ?></p>
								<div class="d-flex justify-content-between align-items-center">
									<div class="btn-group">
										<a class="btn btn-sm btn-outline-secondary" href="<?= getAlbumURL() ?>">View</a>
										<?php if ($_zp_current_album->isMyItem(ALBUM_RIGHTS)): ?>
											<a class="btn btn-sm btn-outline-secondary"
											   href="/zp-core/admin-edit.php?page=edit&album=<?= $_zp_current_album->name ?>">Edit</a>
										<?php endif; ?>
									</div>
									<small class="text-muted"><?= getAlbumDate() ?></small>
								</div>
							</div>
						</div>
					</div>
				<?php endwhile; ?>
			</div>

			<!-- Images -->
			<div class="row">
				<?php while (next_image()): ?>
					<div class="col-md-<?= ceil(12 / getOption("images_per_row")) ?>">
						<div class="card mb-4 box-shadow card-<?= getOption(ThemeOptions::OPTION_COLOR_SCHEME) ? "light" : "dark" ?>">
							<img class="card-img-top" src="<?= getImageThumb() ?>">
							<div class="card-body">
								<div class="card-text d-flex justify-content-between align-items-center">
									<p class="lead"><?= getImageTitle() ?></p>
									<?php if (getOption(ThemeOptions::OPTION_ITEM_TYPES)): ?>
										<p class="lead">
											<small class="text-muted"><?= _("Image") ?></small>
										</p>
									<?php endif; ?>
								</div>
								<p class="card-text"><?= getImageDesc() ?></p>
								<div class="d-flex justify-content-between align-items-center">
									<div class="btn-group">
										<a class="btn btn-sm btn-outline-secondary" href="<?= getCustomImageURL(null, 350, 350, 350, 350, null, null, null, null) ?>">View</a>
										<?php if ($_zp_current_image->isMyItem(ALBUM_RIGHTS)): ?>
											<a class="btn btn-sm btn-outline-secondary"
											   href="/zp-core/admin-edit.php?page=edit&tab=imageinfo&album=<?= $_zp_current_album->name ?>&singleimage=<?= $_zp_current_image->getFileName() ?>">Edit</a>
										<?php endif; ?>
									</div>
									<small class="text-muted"><?= getImageDate() ?></small>
								</div>
							</div>
						</div>
					</div>
				<?php endwhile; ?>
			</div>

			<?php
			printPageListWithNav("« " . gettext("prev"), gettext("next") . " »");
			printTags('links', gettext('<strong>Tags:</strong>') . ' ', 'taglist', '');
			@call_user_func('printGoogleMap');
			@call_user_func('printSlideShowLink');
			@call_user_func('printRating');
			@call_user_func('printCommentForm');
			?>
		</div>

		<?php zp_apply_filter("theme_body_close"); ?>
	</body>
</html>
